// 1.Прототипне наслідування - це можливість використовувати властивості і методи одного об'єкту при створенні іншого об'єкту через посилання в обʼєкті на інший обʼєкт. Таким чином можна утворювати ланцюги об'ктів копіюючи один об'єкт в інший
// 2. Super() використовується в середині методу consntructor для того, щоб викликати конструктор батьківського класу.
// Тоді ласі-нащадкок успадкує властивості і методи батьківського класу.



class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }

  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return +this._salary * 3;
  }
}

let prg1 = new Programmer("Maria", 28, 1000, "english");
let prg2 = new Programmer("Lion", 23, 2000, "spanish");
let prg3 = new Programmer("Sima", 45, 4000, "portugal");

console.log(prg1, prg2, prg3);
